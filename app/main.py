import datetime
import dateutil.parser
from threading import Timer
import socket
import os

from flask import Flask, request
app = Flask(__name__)

@app.errorhandler(500)
def internal_error(error):
    return "Cannot retrieve what you are looking for right now"

@app.route('/', methods=['POST'])
def ai():
    outcome = request.json['outcome']
    scheduled = False
    delta=0
    action=""
    if 'entities' in outcome:
        if 'datetime' in outcome['entities']:
            if len(outcome['entities']['datetime']) > 0:
                if 'value' in outcome['entities']['datetime'][0]:
                    date = dateutil.parser.parse(outcome['entities']['datetime'][0]['value'])
                    deltaTime = date - datetime.datetime.now(datetime.timezone.utc)
                    delta = deltaTime.total_seconds()
                    if (delta < 2):
                        delta = 0
                        scheduled = False
                    else:
                        scheduled = True
        if 'reminder' in outcome['entities']:
            if len(outcome['entities']['datetime']) > 0:
                if 'value' in outcome['entities']['reminder'][0]:
                    action = outcome['entities']['reminder'][0]['value']

    if (action != ""):
        return self.reminder_manager(action, scheduled, delta)
    else:
        return "I don't know what you want."

def reminder_teller(self, value):
    response = "I need to remind you of "
    response += value.replace("my", "your") ##LaClasse
    return response

def scheduled_reminder_teller(self, value):
    action = self.reminder_teller(value)
    os.system("echo \'" + action + "\' | nc localhost 11235")

def reminder_manager(self, value, scheduled=False, delta=0):
    if scheduled == False:
        return self.reminder_teller(value)
    else:
        t = Timer(delta, self.scheduled_reminder_teller, (value,))
        t.start()
        return "I'll remember that !"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
